package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getList() {
        List<Flower> flowers = new ArrayList<>();
        Flower fl = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();

                    if (startElement.getName().getLocalPart().equals("flower")) {
                        fl = new Flower();
                    }
                    //set the other variables from xml elements
                    else if (startElement.getName().getLocalPart().equals("name")) {

                        xmlEvent = xmlEventReader.nextEvent();
                        fl.setName(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("soil")) {

                        xmlEvent = xmlEventReader.nextEvent();
                        fl.setSoil(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("origin")) {

                        xmlEvent = xmlEventReader.nextEvent();
                        fl.setOrigin(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("stemColour")) {

                        xmlEvent = xmlEventReader.nextEvent();
                        fl.setStemColour(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("leafColour")) {

                        xmlEvent = xmlEventReader.nextEvent();
                        fl.setLeafColour(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("aveLenFlower")) {

                        xmlEvent = xmlEventReader.nextEvent();

                        Attribute attr = startElement.getAttributeByName(new QName("measure"));
                        if (attr != null) {
                            fl.setMeasureLen(attr.getValue());
                        }

                        fl.setLenText(Integer.parseInt(xmlEvent.asCharacters().getData()));

                    } else if (startElement.getName().getLocalPart().equals("tempreture")) {

                        xmlEvent = xmlEventReader.nextEvent();

                        Attribute attr = startElement.getAttributeByName(new QName("measure"));
                        if (attr != null) {
                            fl.setMeasureTemp(attr.getValue());
                        }

                        fl.setTempText(Integer.parseInt(xmlEvent.asCharacters().getData()));

                    } else if (startElement.getName().getLocalPart().equals("lighting")) {

                        xmlEvent = xmlEventReader.nextEvent();

                        Attribute attr = startElement.getAttributeByName(new QName("lightRequiring"));
                        if (attr != null) {
                            fl.setLightRequiring(attr.getValue());
                        }

                    } else if (startElement.getName().getLocalPart().equals("watering")) {

                        xmlEvent = xmlEventReader.nextEvent();

                        Attribute attr = startElement.getAttributeByName(new QName("measure"));
                        if (attr != null) {
                            fl.setMeasureWat(attr.getValue());
                        }

                        fl.setTextWat(Integer.parseInt(xmlEvent.asCharacters().getData()));

                    } else if (startElement.getName().getLocalPart().equals("multiplying")) {

                        xmlEvent = xmlEventReader.nextEvent();
                        fl.setMultiplying(xmlEvent.asCharacters().getData());

                    }

                }
                //if flower end element is reached, add flower object to list
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowers.add(fl);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return flowers;
    }

    public void saveSTAXDoc(List<Flower> flowers, String outputFile) {
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = null;
        try {
            xsw = xof.createXMLStreamWriter(new FileWriter(outputFile));
            xsw.setDefaultNamespace("http://www.nure.ua");
            xsw.writeStartDocument();
            xsw.writeStartElement("flowers");
            xsw.writeAttribute("xmlns", "http://www.nure.ua");

            for (Flower fl : flowers) {
                xsw.writeStartElement("flower");

                xsw.writeStartElement("name");
                xsw.writeCharacters(fl.name);
                xsw.writeEndElement();

                xsw.writeStartElement("soil");
                xsw.writeCharacters(fl.soil);
                xsw.writeEndElement();

                xsw.writeStartElement("origin");
                xsw.writeCharacters(fl.origin);
                xsw.writeEndElement();

                xsw.writeStartElement("visualParameters");

                xsw.writeStartElement("stemColour");
                xsw.writeCharacters(fl.stemColour);
                xsw.writeEndElement();

                xsw.writeStartElement("leafColour");
                xsw.writeCharacters(fl.leafColour);
                xsw.writeEndElement();

                xsw.writeStartElement("aveLenFlower");
                xsw.writeAttribute("measure", fl.measureLen);
                xsw.writeCharacters(Integer.toString(fl.lenText));
                xsw.writeEndElement();

                xsw.writeEndElement();

                xsw.writeStartElement("growingTips");

                xsw.writeStartElement("tempreture");
                xsw.writeAttribute("measure", fl.measureTemp);
                xsw.writeCharacters(Integer.toString(fl.tempText));
                xsw.writeEndElement();

                xsw.writeStartElement("lighting");
                xsw.writeAttribute("lightRequiring", fl.lightRequiring);
                xsw.writeEndElement();

                xsw.writeStartElement("watering");
                xsw.writeAttribute("measure", fl.measureWat);
                xsw.writeCharacters(Integer.toString(fl.textWat));
                xsw.writeEndElement();

                xsw.writeEndElement();

                xsw.writeStartElement("multiplying");
                xsw.writeCharacters(fl.multiplying);
                xsw.writeEndElement();

                xsw.writeEndElement();
            }

            xsw.writeEndElement();
            xsw.writeEndDocument();
            xsw.flush();

            format(outputFile);
        } catch (Exception e) {
            System.err.println("Unable to write the file: " + e.getMessage());
        } finally {
            try {
                if (xsw != null) {
                    xsw.close();
                }
            } catch (Exception e) {
                System.err.println("Unable to close the file: " + e.getMessage());
            }
        }

    }

    private static void format(String file) throws TransformerException, IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new InputStreamReader(new FileInputStream(file))));

        Transformer xformer = TransformerFactory.newInstance().newTransformer();

        xformer.setOutputProperty(OutputKeys.METHOD, "xml");

        xformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source source = new DOMSource(document);
        Result result = new StreamResult(new File(file));
        xformer.transform(source, result);
    }
}