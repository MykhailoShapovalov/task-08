package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;
    private final StringBuilder currentValue = new StringBuilder();
    private List<Flower> flowers;
    private Flower currFlower;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void startDocument() {
        flowers = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentValue.setLength(0);

        if (qName.equalsIgnoreCase("flower")) {
            currFlower = new Flower();
        }

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            currFlower.setMeasureLen(attributes.getValue("measure"));
        }

        if (qName.equalsIgnoreCase("tempreture")) {
            currFlower.setMeasureTemp(attributes.getValue("measure"));
        }

        if (qName.equalsIgnoreCase("lighting")) {
            currFlower.setLightRequiring(attributes.getValue("lightRequiring"));
        }

        if (qName.equalsIgnoreCase("watering")) {
            currFlower.setMeasureWat(attributes.getValue("measure"));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {

        if (qName.equalsIgnoreCase("name")) {
            currFlower.setName(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("soil")) {
            currFlower.setSoil(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("origin")) {
            currFlower.setOrigin(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("stemColour")) {
            currFlower.setStemColour(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("leafColour")) {
            currFlower.setLeafColour(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            currFlower.setLenText(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("tempreture")) {
            currFlower.setTempText(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("watering")) {
            currFlower.setTextWat(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("multiplying")) {
            currFlower.setMultiplying(currentValue.toString());
        }

        // end of loop
        if (qName.equalsIgnoreCase("flower")) {
            flowers.add(currFlower);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentValue.append(ch, start, length);
    }

    public List<Flower> getList() {
        return flowers;
    }

    public void saveSAXDoc(List<Flower> flowers, String outputFile) {
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xsw = null;
        try {
            xsw = xof.createXMLStreamWriter(new FileWriter(outputFile));
            xsw.setDefaultNamespace("http://www.nure.ua");
            xsw.writeStartDocument();
            xsw.writeStartElement("flowers");
            xsw.writeAttribute("xmlns", "http://www.nure.ua");

            for (Flower fl : flowers) {
                xsw.writeStartElement("flower");

                xsw.writeStartElement("name");
                xsw.writeCharacters(fl.name);
                xsw.writeEndElement();

                xsw.writeStartElement("soil");
                xsw.writeCharacters(fl.soil);
                xsw.writeEndElement();

                xsw.writeStartElement("origin");
                xsw.writeCharacters(fl.origin);
                xsw.writeEndElement();

                xsw.writeStartElement("visualParameters");

                xsw.writeStartElement("stemColour");
                xsw.writeCharacters(fl.stemColour);
                xsw.writeEndElement();

                xsw.writeStartElement("leafColour");
                xsw.writeCharacters(fl.leafColour);
                xsw.writeEndElement();

                xsw.writeStartElement("aveLenFlower");
                xsw.writeAttribute("measure", fl.measureLen);
                xsw.writeCharacters(Integer.toString(fl.lenText));
                xsw.writeEndElement();

                xsw.writeEndElement();

                xsw.writeStartElement("growingTips");

                xsw.writeStartElement("tempreture");
                xsw.writeAttribute("measure", fl.measureTemp);
                xsw.writeCharacters(Integer.toString(fl.tempText));
                xsw.writeEndElement();

                xsw.writeStartElement("lighting");
                xsw.writeAttribute("lightRequiring", fl.lightRequiring);
                xsw.writeEndElement();

                xsw.writeStartElement("watering");
                xsw.writeAttribute("measure", fl.measureWat);
                xsw.writeCharacters(Integer.toString(fl.textWat));
                xsw.writeEndElement();

                xsw.writeEndElement();

                xsw.writeStartElement("multiplying");
                xsw.writeCharacters(fl.multiplying);
                xsw.writeEndElement();

                xsw.writeEndElement();
            }

            xsw.writeEndElement();
            xsw.writeEndDocument();
            xsw.flush();

            format(outputFile);
        } catch (Exception e) {
            System.err.println("Unable to write the file: " + e.getMessage());
        } finally {
            try {
                if (xsw != null) {
                    xsw.close();
                }
            } catch (Exception e) {
                System.err.println("Unable to close the file: " + e.getMessage());
            }
        }
    }

    private static void format(String file) throws TransformerException, IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new InputStreamReader(new FileInputStream(file))));

        Transformer xformer = TransformerFactory.newInstance().newTransformer();

        xformer.setOutputProperty(OutputKeys.METHOD, "xml");

        xformer.setOutputProperty(OutputKeys.INDENT, "yes");
        Source source = new DOMSource(document);
        Result result = new StreamResult(new File(file));
        xformer.transform(source, result);
    }
}