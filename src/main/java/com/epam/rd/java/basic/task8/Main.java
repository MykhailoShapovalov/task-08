package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

public class Main {

    public static void main(String[] args) {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // Validate XML against XSD
        ////////////////////////////////////////////////////////

        System.out.println("Is xsd file equals xml file: " + validateXMLSchema("input.xsd", "input.xml"));

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE

        List<Flower> flowersDOM = domController.getList();

        // sort (case 1)
        // PLACE YOUR CODE HERE

        System.out.println("DOM sort below(Name):");
        flowersDOM.sort((o1, o2) -> o1.name.compareToIgnoreCase(o2.name));
        System.out.println("FlowersDOM is: " + flowersDOM);

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE

        domController.saveDocFromList(flowersDOM, outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        SAXController saxController = new SAXController(xmlFileName);
        SAXParserFactory factory = SAXParserFactory.newInstance();

        List<Flower> flowersSax = new ArrayList<>();

        try (InputStream is = new FileInputStream(xmlFileName)) {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(is, saxController);

            flowersSax = saxController.getList();

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        // sort  (case 2)
        flowersSax.sort(Comparator.comparing(o -> o.soil));

        // save
        outputXmlFile = "output.sax.xml";
        saxController.saveSAXDoc(flowersSax, outputXmlFile);
        // PLACE YOUR CODE HERE

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        List<Flower> flowersStax = staxController.getList();

        // sort  (case 3)
        flowersStax.sort(Comparator.comparing(o -> o.multiplying));

        // save
        outputXmlFile = "output.stax.xml";
        staxController.saveSTAXDoc(flowersStax, outputXmlFile);
    }

    public static boolean validateXMLSchema(String xsdPath, String xmlPath) {
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
            return false;
        } catch (SAXException e1) {
            System.out.println("SAX Exception: " + e1.getMessage());
            return false;
        }
        return true;
    }
}